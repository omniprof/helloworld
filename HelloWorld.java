// You should use a package but this can be deferred initially
// package com.kenfogel.helloworld;

/**
 * Example of how a HelloWorld program should be written in an object oriented
 * language, in this case Java.
 *
 * @author Ken Fogel
 */
public class HelloWorld {

    /**
     * Work is done in methods of objects, never in the main method
     */
    public void sayHello() {
        System.out.println("Hello World");
    }

    /**
     * Where it all begins but should never be where any work is carried out
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Instantiate an object
        HelloWorld helloWorld = new HelloWorld();
        // Send a message to the object
        helloWorld.sayHello();
        // Exit cleanly
        System.exit(0);
    }
}
